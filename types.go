package main

import (
	"github.com/Pallinder/go-randomdata"
	"github.com/google/uuid"
)

/*
  Request types
*/

type RequestBody struct {
	Rows    int
	Columns []RequestBodyColumn
}

type RequestBodyColumn struct {
	Type   string
	Header string
}

/*
  Generator types
*/

type Column struct {
	Type   ColumnType
	Header string
}

type ColumnType struct {
	Type        string
	Description string
	Generate    ColumnGenerate
}

type ColumnGenerate func() string

var ColumnTypes = []ColumnType{
	ColumnType{
		Type:        "uuid",
		Description: "",
		Generate: func() string {
			return uuid.New().String()
		},
	},
	ColumnType{
		Type:        "first_name",
		Description: "",
		Generate: func() string {
			return randomdata.FirstName(randomdata.RandomGender)
		},
	},
	ColumnType{
		Type:        "last_name",
		Description: "",
		Generate: func() string {
			return randomdata.LastName()
		},
	},
	ColumnType{
		Type:        "silly_name",
		Description: "",
		Generate: func() string {
			return randomdata.SillyName()
		},
	},
	ColumnType{
		Type:        "email",
		Description: "",
		Generate: func() string {
			return randomdata.Email()
		},
	},
	ColumnType{
		Type:        "ip_address",
		Description: "",
		Generate: func() string {
			return randomdata.IpV4Address()
		},
	},
	ColumnType{
		Type:        "street",
		Description: "",
		Generate: func() string {
			return randomdata.Street()
		},
	},
	ColumnType{
		Type:        "city",
		Description: "",
		Generate: func() string {
			return randomdata.City()
		},
	},
	ColumnType{
		Type:        "state",
		Description: "",
		Generate: func() string {
			return randomdata.State(randomdata.Large)
		},
	},
	ColumnType{
		Type:        "country",
		Description: "",
		Generate: func() string {
			return randomdata.Country(randomdata.FullCountry)
		},
	},
	ColumnType{
		Type:        "currency",
		Description: "",
		Generate: func() string {
			return randomdata.Currency()
		},
	},
	ColumnType{
		Type:        "phone",
		Description: "",
		Generate: func() string {
			return randomdata.PhoneNumber()
		},
	},
}
