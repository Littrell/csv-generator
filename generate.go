package main

import (
	"encoding/csv"
	"io/ioutil"
	"os"
)

func generate(rows int, columns []Column) (*os.File, error) {

	data := [][]string{}
	headers := []string{}
	for _, column := range columns {
		headers = append(headers, column.Header)
	}
	data = append(data, headers)

	for i := 0; i < rows; i++ {
		row := []string{}
		for _, column := range columns {
			cell := column.Type.Generate()
			row = append(row, cell)
		}
		data = append(data, row)
	}

	tmpFile, err := ioutil.TempFile("", "tmp*")
	if err != nil {
		return &os.File{}, err
	}

	writer := csv.NewWriter(tmpFile)
	defer writer.Flush()

	for _, value := range data {
		err := writer.Write(value)
		if err != nil {
			return &os.File{}, err
		}
	}

	return tmpFile, nil
}
