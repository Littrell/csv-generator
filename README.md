# csv-generator

**csv-generator** is an API for generating CSV files with random data.

Check out the wiki for [examples](https://gitlab.com/Littrell/csv-generator/-/wikis/Example-Requests).

## Run in Docker

```bash
docker compose up -d && docker compose logs -f
```

Serves off of [http://csvgenerator.localhost](http://csvgenerator.localhost)

## Run locally

### Build

```bash
./build.sh
```

### Run

```bash
./server
```

Serves off of [http://localhost:9000](http://localhost:9000)
