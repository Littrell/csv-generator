module gitlab.com/Littrell/csv-generator

go 1.14

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/golang/gddo v0.0.0-20210115222349-20d68f94ee1f
	github.com/google/uuid v1.2.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	golang.org/x/text v0.3.6 // indirect
)
